var gulp = require('gulp');

var livereload = require('gulp-livereload');
var connect = require('gulp-connect');


gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

gulp.task('html', function() {
    gulp.src('./*.html')
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('./*.html', ['html']);
});


gulp.task('default', ['watch', 'connect']);
